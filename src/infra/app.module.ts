import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.modules';
import { HttpModule } from './http/http.modules';

@Module({
  imports: [HttpModule, DatabaseModule],
})
export class AppModule {}
