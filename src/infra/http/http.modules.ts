import { CancelNotification } from '@application/use-cases/cancelNotification';
import { CountRecipientIdNotification } from '@application/use-cases/countRecipientIdNotification';
import { GetRecipientIdNotification } from '@application/use-cases/getRecipientIdNotitification';
import { ReadNotification } from '@application/use-cases/read-notification';
import { SendNotification } from '@application/use-cases/SendNotification';
import { UnreadNotification } from '@application/use-cases/unread-notification';
import { DatabaseModule } from '@infra/database/database.modules';
import { Module } from '@nestjs/common';
import { NotificationsControllers } from './controllers/notifications.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [NotificationsControllers],
  providers: [
    SendNotification,
    CancelNotification,
    ReadNotification,
    UnreadNotification,
    CountRecipientIdNotification,
    GetRecipientIdNotification,
  ],
})
export class HttpModule {}
