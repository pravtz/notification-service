import { CancelNotification } from '@application/use-cases/cancelNotification';
import { CountRecipientIdNotification } from '@application/use-cases/countRecipientIdNotification';
import { GetRecipientIdNotification } from '@application/use-cases/getRecipientIdNotitification';
import { ReadNotification } from '@application/use-cases/read-notification';
import { SendNotification } from '@application/use-cases/SendNotification';
import { UnreadNotification } from '@application/use-cases/unread-notification';
import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { CreateNotificationBoby } from '../dtos/create-notification-body';
import { NotificationViewModel } from '../view-models/notification-view-model';

@Controller('notifications')
export class NotificationsControllers {
  constructor(
    private sendNotitication: SendNotification,
    private cancelNotification: CancelNotification,
    private readNotification: ReadNotification,
    private unreadNotification: UnreadNotification,
    private countRecipientIdNotification: CountRecipientIdNotification,
    private getRecipientIdNotification: GetRecipientIdNotification,
  ) {}

  @Patch(':id/cancel')
  async cancel(@Param('id') id: string) {
    await this.cancelNotification.execute({
      notificationId: id,
    });
  }

  @Get('count/from/:recipientId')
  async countFromRecipient(@Param('recipientId') recipientId: string) {
    const { count } = await this.countRecipientIdNotification.execute({
      recipientId,
    });
    return {
      count,
    };
  }

  @Get('from/:recipientId')
  async getFromRecipient(@Param('recipientId') recipientId: string) {
    const { notifications } = await this.getRecipientIdNotification.execute({
      recipientId,
    });

    return notifications.map(NotificationViewModel.toHTTP);
  }

  @Patch(':id/read')
  async read(@Param('id') id: string) {
    await this.readNotification.execute({
      notificationId: id,
    });
  }

  @Patch(':id/unread')
  async unread(@Param('id') id: string) {
    await this.unreadNotification.execute({
      notificationId: id,
    });
  }

  @Post()
  async create(@Body() body: CreateNotificationBoby) {
    const { recipientId, content, category } = body;

    const { notification } = await this.sendNotitication.execute({
      recipientId,
      content,
      category,
    });
    return NotificationViewModel.toHTTP(notification);
  }
}
