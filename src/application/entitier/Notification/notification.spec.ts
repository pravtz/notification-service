import { Content } from './content';
import { Notification } from './Notification';

describe('Notification', () => {
  it('should be acle to create a notification content', () => {
    const notification = new Notification({
      content: new Content('teste da notificação'),
      category: 'social',
      recipientId: 'exemple-recepent-id',
    });

    expect(notification).toBeTruthy();
  });
});
