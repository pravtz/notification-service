import { Injectable } from '@nestjs/common';
import { NotificationRepository } from '../repositories/NotificationRepositories';

interface CountRecipientIdNotificationRequeste {
  recipientId: string;
}

interface CountRecipientIdNotificationResponse {
  count: number;
}

@Injectable()
export class CountRecipientIdNotification {
  constructor(private notificationsRepository: NotificationRepository) {}

  async execute(
    request: CountRecipientIdNotificationRequeste,
  ): Promise<CountRecipientIdNotificationResponse> {
    const { recipientId } = request;

    const count = await this.notificationsRepository.countManyByRecipientId(
      recipientId,
    );

    return {
      count,
    };
  }
}
