import { makeNotificationFactory } from '@test/factories/notification-factory';
import { InMemoryNotificationsRepository } from '../../../test/repositories/in-memory-notification-repository';
import { CountRecipientIdNotification } from './countRecipientIdNotification';

describe('Count Notification', () => {
  it('should be able to count the number of notifications', async () => {
    const notificationsRepository = new InMemoryNotificationsRepository();
    const countRecipientIdNotification = new CountRecipientIdNotification(
      notificationsRepository,
    );

    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId' }),
    );
    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId' }),
    );
    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId2' }),
    );

    const { count } = await countRecipientIdNotification.execute({
      recipientId: 'example-recipientId',
    });

    expect(count).toEqual(2);
  });
});
