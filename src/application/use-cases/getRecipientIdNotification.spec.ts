import { makeNotificationFactory } from '@test/factories/notification-factory';
import { InMemoryNotificationsRepository } from '../../../test/repositories/in-memory-notification-repository';
import { GetRecipientIdNotification } from './getRecipientIdNotitification';

describe('Get RecipientId Notification', () => {
  it('should be able to return notifications related to recipientId', async () => {
    const notificationsRepository = new InMemoryNotificationsRepository();
    const getRecipientIdNotification = new GetRecipientIdNotification(
      notificationsRepository,
    );

    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId' }),
    );
    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId' }),
    );
    await notificationsRepository.create(
      makeNotificationFactory({ recipientId: 'example-recipientId2' }),
    );

    const { notifications } = await getRecipientIdNotification.execute({
      recipientId: 'example-recipientId',
    });

    expect(notifications).toHaveLength(2);
    expect(notifications).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ recipientId: 'example-recipientId' }),
        expect.objectContaining({ recipientId: 'example-recipientId' }),
      ]),
    );
  });
});
