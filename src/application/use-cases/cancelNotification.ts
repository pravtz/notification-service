import { Injectable } from '@nestjs/common';
import { NotificationRepository } from '../repositories/NotificationRepositories';
import { NotificationNotFound } from './errors/notification-not-found';

interface CancelNotificationRequeste {
  notificationId: string;
}

type CancelNotificationResponse = void;

@Injectable()
export class CancelNotification {
  constructor(private notificationsRepository: NotificationRepository) {}

  async execute(
    request: CancelNotificationRequeste,
  ): Promise<CancelNotificationResponse> {
    const { notificationId } = request;

    const notification = await this.notificationsRepository.findById(
      notificationId,
    );

    if (!notification) {
      throw new NotificationNotFound();
    }

    notification.cancel();
    await this.notificationsRepository.save(notification);
  }
}
