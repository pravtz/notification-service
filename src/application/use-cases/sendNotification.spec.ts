import { InMemoryNotificationsRepository } from '../../../test/repositories/in-memory-notification-repository';
import { SendNotification } from './SendNotification';

describe('Send Notification', () => {
  it('should be able to send a notification', async () => {
    const notificationsRepository = new InMemoryNotificationsRepository();
    const sendNotification = new SendNotification(notificationsRepository);

    await sendNotification.execute({
      content: 'This is Notification',
      category: 'anyware',
      recipientId: 'example',
    });

    expect(notificationsRepository.notifications).toHaveLength(1);
  });
});
