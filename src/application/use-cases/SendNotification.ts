import { Injectable } from '@nestjs/common';
import { Content } from '../entitier/Notification/content';
import { Notification } from '../entitier/Notification/Notification';
import { NotificationRepository } from '../repositories/NotificationRepositories';

interface SendNotificationRequeste {
  content: string;
  category: string;
  recipientId: string;
}

interface SendNotificationResponse {
  notification: Notification;
}

@Injectable()
export class SendNotification {
  constructor(private notificationsRepository: NotificationRepository) {}

  async execute(
    request: SendNotificationRequeste,
  ): Promise<SendNotificationResponse> {
    const { recipientId, content, category } = request;

    const notification = new Notification({
      recipientId,
      content: new Content(content),
      category,
    });

    await this.notificationsRepository.create(notification);

    return {
      notification,
    };
  }
}
