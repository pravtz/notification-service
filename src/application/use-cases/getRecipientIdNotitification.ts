import { Injectable } from '@nestjs/common';
import { NotificationRepository } from '@application/repositories/NotificationRepositories';
import { Notification } from '@application/entitier/Notification/Notification';

interface GetRecipientIdNotificationRequeste {
  recipientId: string;
}

interface GetRecipientIdNotificationResponse {
  notifications: Notification[];
}

@Injectable()
export class GetRecipientIdNotification {
  constructor(private notificationsRepository: NotificationRepository) {}

  async execute(
    request: GetRecipientIdNotificationRequeste,
  ): Promise<GetRecipientIdNotificationResponse> {
    const { recipientId } = request;

    const notifications =
      await this.notificationsRepository.findManyByRecipientId(recipientId);

    return {
      notifications,
    };
  }
}
