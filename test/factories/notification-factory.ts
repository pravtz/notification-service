import { Content } from '@application/entitier/Notification/content';
import {
  Notification,
  INotification,
} from '@application/entitier/Notification/Notification';

type Override = Partial<INotification>;

export function makeNotificationFactory(override: Override = {}) {
  return new Notification({
    category: 'nova categoria',
    content: new Content('Novo conteúdo para teste'),
    recipientId: 'example-recipientId',
    ...override,
  });
}
