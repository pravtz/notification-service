import { Notification } from 'src/application/entitier/Notification/Notification';
import { NotificationRepository } from 'src/application/repositories/NotificationRepositories';

export class InMemoryNotificationsRepository implements NotificationRepository {
  public notifications: Notification[] = [];

  async findManyByRecipientId(recipientId: string): Promise<Notification[]> {
    const findRecipientNotificationById = this.notifications.filter(
      (item) => item.recipientId === recipientId,
    );

    return findRecipientNotificationById;
  }

  async create(notification: Notification) {
    this.notifications.push(notification);
  }

  async findById(notificationId: string): Promise<Notification | null> {
    const findNotificationById = this.notifications.find(
      (item) => item.id === notificationId,
    );
    if (!findNotificationById) {
      return null;
    }
    return findNotificationById;
  }

  async save(notification: Notification): Promise<void> {
    const notificationIndex = this.notifications.findIndex(
      (item) => item.id === notification.id,
    );

    if (notificationIndex >= 0) {
      this.notifications[notificationIndex] = notification;
    }
  }

  async countManyByRecipientId(recipientId: string): Promise<number> {
    const notificationFilter = this.notifications.filter(
      (item) => item.recipientId === recipientId,
    );
    return notificationFilter.length;
  }
}
