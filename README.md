# API Notification

## Description

This is api notification

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

> colection json insominia
> [insominia_collection](./doc/Insomnia_notification.json)

## Routs

---

### Create notification

`localhost:3000/notifications/`

```Bash
#example

  curl \
  -X POST \
  -d '{"content": "anyware","recipientId": "c527fabe-5af6-48c2-9f9b-295ea4ff28e2","category": "anyware"}' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -i \
  http://localhost:3000/notifications/

```

---

### Read notifications with

BASE/count/from/:recipientId

---

### Cancel notifications

`localhost:3000/:id/cancel`

```Bash
#example with id = 5baf7e4b-d142-49ec-b740-07991542e9a3

  curl \
  -X PATCH \
  -H 'Content-Type: application/json' \
  http://localhost:3000/notifications/5baf7e4b-d142-49ec-b740-07991542e9a3/cancel

```

---

### Get notifications recipient

Fetch notifications with the memo id of the recipient

**methodo:**: GET

`localhost:3000/from/:recipientId`

```Bash
#example with recipientId = c527fabe-5af6-48c2-9f9b-295ea4ff28e2

  curl \
  -X GET \
  -H 'Content-Type: application/json' \
  http://localhost:3000/notifications/from/c527fabe-5af6-48c2-9f9b-295ea4ff28e2
```
